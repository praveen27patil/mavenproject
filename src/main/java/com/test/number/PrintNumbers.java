package com.test.number;

import java.util.stream.IntStream;

public class PrintNumbers {

	public static void main(String[] args) {
		
		//range() exclude last number, where rangeClosded() includes last numbebr
		IntStream.rangeClosed(1, 10).forEach(
				nbr -> System.out.println(nbr)
				);
	}

}
